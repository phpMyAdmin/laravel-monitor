// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import VueRouter from 'vue-router'
import store from './store'
import routes from './router'

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-default/index.css'
import './styles/index.scss'
Vue.use(ElementUI)

Vue.use(VueRouter)

import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

import * as filters from './filters'
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

import axios from 'axios'
window.baseApiUrl = '/api/v1/'
axios.defaults.baseURL = window.baseApiUrl
axios.interceptors.request.use(function (config) {
  config.timeout = 15000 // 设置默认超时时间
  return config
}, function (error) {
  console.log(error)
  return Promise.reject(error)
})
axios.interceptors.response.use((response) => {
  return response
}, (error) => {
  console.log(error)
  let errormsg = ''
  let status = error.response.status
  if (status === 403) {
    errormsg = '未登录，请先登录'
  }
  if (errormsg !== '') {
    window.toastr.error(errormsg)
    router.replace({ path: '/login' })
  }
  return Promise.reject(error)
})

Vue.prototype.$http = axios
Vue.config.productionTip = false

var router = new VueRouter({
  routes: routes,
  mode: 'hash',
  linkActiveClass: 'nav-link-active',
  // mode: 'history',
  scrollBehavior: function (to, from, savedPosition) {
    return savedPosition || { x: 0, y: 0 }
  }
})
router.beforeEach((to, from, next) => {
  NProgress.start()
  next()
})
router.afterEach(() => {
  NProgress.done()
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router: router,
  store,
  template: '<App/>',
  components: { App }
})
