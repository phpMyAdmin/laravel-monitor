import Layout from '@/views/layout/Layout'

const routes = [
  {
    path: '/login',
    name: '登陆',
    component: resolve => require(['../views/login/index'], resolve)
  },
  {
    path: '/',
    name: '控制台',
    component: Layout,
    showInMenu: true,
    children: [
      {
        path: 'dashboard',
        name: '控制台',
        component: resolve => require(['../views/dashboard/index'], resolve)
      }, {
        path: 'checkpage/create',
        name: '创建监控项',
        component: resolve => require(['../views/checkpage/create'], resolve)
      }, {
        path: 'checkpage/history/:id',
        name: '监控项历史记录',
        component: resolve => require(['../views/checkpage/history'], resolve)
      }, {
        path: 'checkpage/edit/:id',
        name: '创建监控项',
        component: resolve => require(['../views/checkpage/edit'], resolve)
      }, {
        path: 'checkpage',
        name: '监控项列表',
        showInMenu: true,
        component: resolve => require(['../views/checkpage/index'], resolve),
        children: [
          {
            path: 'index',
            name: '列表',
            showInMenu: true,
            component: resolve => require(['../views/checkpage/index'], resolve)
          },
          {
            path: 'create',
            name: '创建',
            showInMenu: true,
            component: resolve => require(['../views/checkpage/create'], resolve)
          }
        ]
      }
    ]
  }
]
export default routes
