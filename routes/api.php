<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::any('/v1/login', 'Api\LoginController@login');

Route::any('sql/index', 'Api\SqlController@index');
Route::any('sql/prebuild', 'Api\SqlController@prebuild');
Route::any('sql/create', 'Api\SqlController@create');
Route::any('sql/query/{id}', 'Api\SqlController@query');
Route::get('sql/{id}', 'Api\SqlController@show');
Route::delete('sql/{id}', 'Api\SqlController@destroy');

Route::group(['prefix' => '/v1' , 'namespace' => 'Api' ], function () {
    Route::get('checkpage/history/{id}', 'CheckPageController@history');
    Route::resource('checkpage', 'CheckPageController', ['only'=>['create','store','index','show','destroy','update']]);

    Route::any('sql/prebuild', 'SqlController@prebuild');
});