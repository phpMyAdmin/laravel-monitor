<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
//    $redis = app()->redis();
    $data = array();
    $action = 'test';
    $time = date('Y-m-d H:i:s');
    $path = request()->path();
    $method = request()->method();
    $cost_time = round(microtime(true)-LARAVEL_START,2);
    $data[] = $time;
    $data[] = $action;
    $data[] = $path;
    $data[] = $method;
    $data[] = $cost_time;
    $data[] = request()->getClientIp();
    $data[] = request()->header('user_agent');
    $data = implode(' | ',$data);
    echo $data;exit;
    Redis::set('test','test');
    Redis::lpush('logstash.php_action',$data);
    return view('welcome');
});
