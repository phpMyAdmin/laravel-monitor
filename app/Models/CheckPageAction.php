<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CheckPageAction extends Model {
    protected $fillable = [
        'cp_id',
        'type',
        'dom',
    ];
}
