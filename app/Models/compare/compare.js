var fs = require( 'fs' );
var ROOT = fs.absolute( fs.workingDirectory + '' );

//路径可能有问题
if( ROOT.indexOf('public')!=-1 ){
    ROOT += '/..';
}
ROOT +=  '/app/Models/compare';

var _mismatchTolerance = 0.05;
var _resembleOutputSettings =  {
            errorColor: {
                red: 255,
                green: 0,
                blue: 0
            },
            errorType: 'movement',
            // errorType: 'flat',
            transparency: 0.5
        };
var _addLabelToFailedImage = false;
var _waitTimeout = 100000;
var _failures = '.' + fs.separator + 'failures';
var _diffImageSuffix = ".diff";
var _failureImageSuffix = ".fail";
var outputFilename = casper.cli.get("output") || "output.json";
_resembleContainerPath = ROOT+'/resemblejscontainer.html';
_resemblePath = ROOT+'/resemblejs/resemble.js';
console.log(fs.workingDirectory,ROOT,outputFilename,_resemblePath);

// var baseFile = '/home/vagrant/test/casperjs/s3.jpg';
// var file = '/home/vagrant/test/casperjs/s4.jpg';

var baseFile = casper.cli.get('basefile');
var file = casper.cli.get('file');
var diffOutput = casper.cli.get('diffoutput');
var diffLimitPercent = casper.cli.get('diff') || '0.1';

function _isFile( path ) {
    var exists = false;
    try {
        exists = fs.isFile( path );
    } catch ( e ) {
        if ( e.name !== 'NS_ERROR_FILE_TARGET_DOES_NOT_EXIST' && e.name !== 'NS_ERROR_FILE_NOT_FOUND' ) {
            // We weren't expecting this exception
            throw e;
        }
    }
    return exists;
}
function _onFail( test ) {
    console.dir(test);
    fs.write(outputFilename, JSON.stringify(test, null, 4), 'w');
    // fs.writeFile(outputFilename, JSON.stringify('ffasdf', null, 4), function(err) {
    //     if(err) {
    //       console.log(err);
    //     } else {
    //       console.log("result saved to " + outputFilename);
    //     }
    // });
    // casper.test.fail( 'Visual change found for screenshot ' + test.filename + ' (' + test.mismatch + '% mismatch)' );
}
function _onPass( test ) {
    console.log( '\n' );
    fs.write(outputFilename, JSON.stringify(test, null, 4), 'w');
    casper.test.pass( 'No changes found for screenshot ' + test.filename );
}
function initClient() {
    casper.page.injectJs( _resemblePath );
    casper.evaluate( function ( mismatchTolerance, resembleOutputSettings ) {
            var result;
            var div = document.createElement( 'div' );
            // this is a bit of hack, need to get images into browser for analysis
            div.style = "display:block;position:absolute;border:0;top:10px;left:0;";
            // div.style = "display:block;position:absolute;border:0;top:0;left:0;height:1px;width:1px;";
            div.innerHTML = '<form id="image-diff-form">' +
                '<input type="file" id="image-diff-one" name="one"/>' +
                '<input type="file" id="image-diff-two" name="two"/>' +
                '</form><div id="image-diff"></div>';
            document.body.appendChild( div );
            if ( resembleOutputSettings ) {
                resemble.outputSettings( resembleOutputSettings );
            }
            window._imagediff_ = {
                hasResult: false,
                hasImage: false,
                run: run,
                getResult: function () {
                    window._imagediff_.hasResult = false;
                    return result;
                }
            };
            function run( label ) {
                function render( data ) {
                    var img = new Image();
                    img.onload = function () {
                        window._imagediff_.hasImage = true;
                    };
                    document.getElementById( 'image-diff' ).appendChild( img );
                    img.src = data.getImageDataUrl( label );
                }
                resemble( document.getElementById( 'image-diff-one' ).files[ 0 ] ).
                compareTo( document.getElementById( 'image-diff-two' ).files[ 0 ] ).
                ignoreAntialiasing(). // <-- muy importante
                onComplete( function ( data ) {
                    var diffImage;
                    if ( Number( data.misMatchPercentage ) > mismatchTolerance ) {
                        result = data.misMatchPercentage;
                    } else {
                        result = false;
                    }
                    window._imagediff_.hasResult = true;
                    if ( Number( data.misMatchPercentage ) > mismatchTolerance ) {
                        render( data );
                    }
                } );
            }
        },
        _mismatchTolerance,
        _resembleOutputSettings
    );
}
function asyncCompare( one, two, func ) {
    console.log('asyncCompare start');
    if ( !casper.evaluate( function () {
            return window._imagediff_;
        } ) ) {
        initClient();
    }
    casper.fillSelectors( 'form#image-diff-form', {
        '[name=one]': one,
        '[name=two]': two
    } );
    casper.evaluate( function ( filename ) {
        window._imagediff_.run( filename );
    }, {
        label: _addLabelToFailedImage ? one : false
    } );
    casper.waitFor(
        function check() {
            return this.evaluate( function () {
                return window._imagediff_.hasResult;
            } );
        },
        function () {
            var mismatch = casper.evaluate( function () {
                return window._imagediff_.getResult();
            } );
            if ( Number( mismatch ) ) {
                func( false, mismatch );
            } else {
                func( true );
            }
        },
        function () {
            func( false );
        },
        _waitTimeout
    );
}
// var casper = require('casper').create({
//     logLevel: 'debug',
//      pageSettings: {
//             loadImages: false, // do not load images
//             loadPlugins: false // do not load NPAPI plugins (Flash, Silverlight, ...)
//         },
//     verbose:true
// });
_resembleContainerPath = ROOT+'/resemblejscontainer.html';
//casper.start('http://casperjs.org/');
casper.start();
casper.thenOpen( 'about:blank', function () {
    this.echo('open start');
}); // reset page (fixes bug where failure screenshots leak between captures)
casper.thenOpen( 'file:///' + _resembleContainerPath, function () {    
    var test = {
        filename: baseFile
    };
    asyncCompare( baseFile, file, function ( isSame, mismatch ) {
                if ( !isSame ) {
                    // test.fail = true;
                    casper.waitFor(
                        function check() {
                            return casper.evaluate( function () {
                                return window._imagediff_.hasImage;
                            } );
                        },
                        function () {
                            var failFile, safeFileName, increment;
                            if ( _failures ) {
                                // flattened structure for failed diffs so that it is easier to preview
                                failFile = _failures + fs.separator + file.split( /\/|\\/g ).pop().replace( _diffImageSuffix + '.png', '' ).replace( '.png', '' );
                                safeFileName = failFile;
                                increment = 0;
                                while ( _isFile( safeFileName + _failureImageSuffix + '.png' ) ) {
                                    increment++;
                                    safeFileName = failFile + '.' + increment;
                                }
                                failFile = safeFileName + _failureImageSuffix + '.png';
                                if( diffOutput ){
                                    failFile = diffOutput;
                                }
                                console.log(mismatch);
                                if( mismatch>diffLimitPercent ){
                                    console.log(mismatch,'great then',diffLimitPercent);
                                    // casper.captureSelector( failFile, 'img' );
                                    console.log( 'Failure! Saved to ' + failFile );
                                }
                                test.failFile = failFile;
                                casper.captureSelector( failFile, 'img' );
                            }
                            if ( file.indexOf( _diffImageSuffix + '.png' ) !== -1 ) {
                                casper.captureSelector( file.replace( _diffImageSuffix + '.png', _failureImageSuffix + '.png' ), 'img' );
                            } else {
                                // casper.captureSelector( file.replace( '.png', _failureImageSuffix + '.png' ), 'img' );
                            }
                            casper.evaluate( function () {
                                window._imagediff_.hasImage = false;
                            } );
                            if ( mismatch ) {
                                console.log('test',mismatch);
                                test.mismatch = mismatch;
                                
                                _onFail( test ); // casper.test.fail throws and error, this function call is aborted

                                return; // Just to make it clear what is happening
                            } else {
                                _onTimeout( test );
                            }
                        },
                        function () {},
                        _waitTimeout
                    );
                } else {
                    test.success = true;
                    _onPass( test );
                }
            } );
});
casper.run();
