<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CheckPageHistory extends Model {
    public $table = 'check_page_historys';
    protected $fillable = [
        'cp_id',
        'action_id',
        'result',
        'last_result',
        'diff_result',
        'diff_percent'
    ];
}
