<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CheckPage extends Model {
    protected $fillable = [
        'title' ,
        'url' ,
        'crontab_period' ,
        'is_enabled' ,
        'user_agent' ,
        'view_port' ,
        'config' ,
        'last_cost_time' ,
        'last_run_at'
    ];

    public function actions(){
        return $this->hasMany('\App\Models\CheckPageAction','cp_id','id');
    }
}
