<?php

namespace App\Console\Commands;

use App\Models\PageMonitor;
use Illuminate\Console\Command;

class TestCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $monitor = new PageMonitor();
        $time = microtime(true);
        $img1 = public_path() .'/upload/capture/2017-06-10/4593bccccd5223.png';
        $img2 = public_path() .'/upload/capture/2017-06-10/4593bc589776ec.png';
        $diffImg = public_path() .'/upload/capture/2017-06-10/diff.png';
        $result = $monitor->compareImage($img1 , $img2 , $diffImg);
        dd($result);
    }
}
