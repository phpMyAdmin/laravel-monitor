<?php

namespace App\Console\Commands;

use App\Models\CheckPageAction;
use App\Models\CheckPageHistory;
use Illuminate\Console\Command;
use App\Models\PageMonitor;
use Storage;

class CheckPage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'job:checkpage {job_id : job_id }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '检查页面';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $job_id = $this->argument('job_id');
        $this->info(__CLASS__. 'job id: '.$job_id);
        $job = \App\Models\CheckPage::find($job_id);
        if( empty($job) ){
            $this->error("任务{$job_id}配置获取失败");
            return true;
        }
//        dd($job);
        $actions = $job->actions;
//        foreach($actions as $action){
//            $type = $action->type;
//            $params = $action->dom;
//            dd($type,$params);
//        }
        $pageMonitor = new PageMonitor();
        $pageMonitor->setUserAgent($job->user_agent);
        list($width , $height) = explode('*' , $job->view_port);
        $pageMonitor->setViewPort($width , $height);
        $pageMonitor->start($job->url);
//        $pageMonitor->setDebug(true);
        $tmpActionIds = array();
        $index = 0;
        foreach($actions as $action){
            $type = $action->type;
            $params = $action->dom;
            $pageMonitor->$type($params);
            //只有以下几种才会有结果输出
            if( !in_array($type , array('captureSelector','capturePage','fetchText')) ){
                continue;
            }
            $tmpActionIds[$index] = $action->id;
            $index++;
        }
        $pageMonitor->run();
        $results = $pageMonitor->getResult();
        $urls = $pageMonitor->getRequestedUrls();
        $index = 0;
        if( empty($results['actions']) ){
            logger()->error(__CLASS__.' 执行失败', $results);
            return true;
        }
        foreach($results['actions'] as $result){
            $job_id = $job->id;
            $action_id = $tmpActionIds[$index];
            $action_detail = CheckPageAction::where('id',$action_id)->first();
            $data = array(
                'cp_id'     => $job->id,
                'action_id' => $tmpActionIds[$index],
                'result'    => $result,
            );
            logger()->info($data);
            $capturePath = public_path() . '/upload/capture/' . date('Y-m-d');
            if ( !file_exists($capturePath) ) {
                mkdir($capturePath , '0775' , true);
            }
            //获取上一次的运行结果
            $last_action_result = CheckPageHistory::where('cp_id',$job_id)->where('action_id',$action_id)->orderBy('id','desc')->first();
            if( !empty($last_action_result) ){
                $data['last_result'] = $last_action_result->result;
                //如果是图片类型
                if( in_array($action_detail->type, array('captureSelector','capturePage')) ){
                    $last_file_size =  filesize(public_path().$data['last_result']);
                    if( !file_exists($result) ){
                        continue;
                    }
                    if( filesize($result) != $last_file_size ){
                        $new_file_name = $capturePath.'/'.uniqid($action_id).'.png';
                        copy($result,$new_file_name);
                        $data['result'] = str_replace(public_path() , '',$new_file_name);
                    }else{
                        $data['result'] = $data['last_result'];
                        $data['diff_result'] = '';
                        $data['diff_percent'] = 0;
                        unlink($result);//删除无用文件
                    }
                }
            }else{
                //如果是图片类型
                if( in_array($action_detail->type, array('captureSelector','capturePage')) ) {
                    $new_file_name = $capturePath . '/' . uniqid($action_id) . '.png';
//                Storage::move($result,$new_file_name);
                    copy($result , $new_file_name);
                    $data['result'] = str_replace(public_path() , '' , $new_file_name);
                }
            }
            CheckPageHistory::create($data);
            $index++;
        }
        dd($result,$urls);
    }
}
