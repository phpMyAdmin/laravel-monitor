<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;

class LoginController extends BaseApiController {

    /*登陆*/
    public function login(Request $request)
    {
        $input = $request->all();
        if (!$token = JWTAuth::attempt($input)) {
            return $this->apiReturn(false,'用户名或密码错误');
        }
        $user = JWTAuth::toUser($token);
//        $role = $user->roles()->first();
//        if( empty($role) ){
//            return $this->apiReturn(false,'请联系管理员设置身份信息');
//        }
//        $CheckPages = $role->CheckPages()->get()->toArray();
//        if( empty($CheckPages) ){
//            return $this->apiReturn(false,'请联系管理员设置权限信息');
//        }
//        $CheckPages = array_pluck($CheckPages,'name');
//        unset($user->password);
        return $this->apiReturn(true,'ok',['token'=>$token,'user'=>$user]);
    }
}
