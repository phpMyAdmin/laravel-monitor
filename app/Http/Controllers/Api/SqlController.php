<?php

namespace App\Http\Controllers\Api;

use App\Models\Sql;
use Illuminate\Http\Request;
use DB;
use League\Flysystem\Exception;

class SqlController extends BaseApiController
{
    public function index() {
        $data = Sql::orderBy('id', 'desc')->get();
        return response(array('status' => 1, 'data' => $data));
    }

    public function prebuild(Request $request) {
        if (!$request->has('sql')) {
            return response()->json(['status' => - 1, 'msg' => 'sql is null']);
        }
        $sql = $request->input('sql');
        preg_match_all('/{\$\w+}/', $sql, $match);
        if (empty($match[0])) {
            return response()->json(['sql' => $sql]);
        }
        $querys = array();
        foreach ((array)$match[0] as $item) {
            $item = preg_replace('/[{$|}\W]/', '', $item);
            $inputArr['placeholder'] = $item;
            $inputArr['label'] = $item;
            $inputArr['name'] = $item;
            $inputArr['type'] = 'text';
            $inputArr['default'] = '';
            $inputArr['value'] = '';
            $querys[$item] = $inputArr;
        }
        return response()->json(['status' => 1, 'sql' => $sql, 'querys' => $querys]);
    }

    public function create(Request $request) {
        $this->validate($request, ['name' => 'required', 'sql' => 'required']);
        $data = $request->all();
        if (!empty($data['querys'])) {
            $data['querys'] = serialize($data['querys']);
        }
        $result = Sql::create($data);
        dd($result);
    }

    public function show($id) {
        $sql = Sql::find($id);
        $sql['querys'] = unserialize($sql['querys']);
        return response()->json(['status' => 1, 'data' => $sql]);
    }

    public function query($id, Request $request) {
        $query = Sql::find($id);
        $sql = $query['sql'];
        $query_params = unserialize($query['querys']);
        $sqlMaxCount = 10;
        $pageStart = intval($request->input('page',1));
        $pageSize = 10;
        foreach ((array)$query_params as $param => $config) {
            $value = '';
            if ($request->has($param)) {
                $value = $request->input($param);
            }
            $sql = str_replace('{$' . $param . '}', $value, $sql);
        }
        $sql = strtolower($sql);
        $start_time = microtime(true);
        //获取查询总条数，如果太多则分页
        $sqlFromPosition = strpos($sql,'from');
        $sqlCountStr = 'select count(*) as count '.substr($sql,$sqlFromPosition);
        $queryResult = DB::select($sqlCountStr);
        $sqlRowsCount = $queryResult[0]->count;
        if( $sqlRowsCount>$sqlMaxCount ){
            $limitFrom = ($pageStart-1)*$pageSize;
            $rows = DB::select($sql." limit $limitFrom,$pageSize");
        }else{
            $rows = DB::select($sql);
        }
        $cost_time = round(microtime(true) - $start_time, 2);
        $columns = array();
        if (!empty($rows) && is_array($rows) ) {
           $row_header = $rows[0];
           foreach($row_header as $key=>$value){
               $columns[] = array('prop' => $key ,'label'=> $key);
           }
        }
        $data = array(
            'sql'       => $sql,
            'count'     => $sqlRowsCount,
            'cost_time' => $cost_time,
            'rows'      => $rows,
            'columns'   => $columns,
        );
        return response()->json(['status' => 1, 'data' => $data]);
    }

    public function destroy($id) {
        Sql::destroy($id);
        return response()->json(['status' => 1]);
    }
}
