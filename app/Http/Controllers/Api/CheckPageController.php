<?php

namespace App\Http\Controllers\Api;

use App\Models\CheckPageHistory;
use Illuminate\Http\Request;
use App\Models\CheckPage;
use App\Models\CheckPageAction;

class CheckPageController extends BaseApiController {
    public function index() {
        $checkPages = CheckPage::paginate(15);
        return $this->apiReturn(true , 'ok' , $checkPages);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request) {
        $this->validate($request , ['title' => 'required' , 'url' => 'required']);

        $checkPage = CheckPage::create($request->all());
        if ( !empty($request->input('actions')) && is_array($request->input('actions')) ) {
            foreach ( $request->input('actions') as $action ) {
                $action['cp_id'] = $checkPage->id;
                CheckPageAction::create($action);
            }
        }
        return $this->apiReturn(true , 'ok' , compact('checkPage'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show($id) {
        $checkpage = CheckPage::with('actions')->findOrFail($id);
//        $actions = $checkpage->actions->toArray();
//        dd(array_pluck($actions,'id'));
        return $this->apiReturn(true , 'ok' , $checkpage);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int                      $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id , Request $request) {
        $this->validate($request , ['title' => 'required' , 'url' => 'required']);

        $checkPage = CheckPage::findOrFail($id);
        $checkPage->update($request->all());
        if ( !empty($request->input('actions')) && is_array($request->input('actions')) ) {
            $actionsIdsHave = array_pluck($checkPage->actions->toArray() , 'id');
            $actionsIdsPut = array();
            foreach ( $request->input('actions') as $action ) {
                if ( empty($action['id']) ) {
                    $action['cp_id'] = $checkPage->id;
                    CheckPageAction::create($action);
                } else {
                    $actionModel = CheckPageAction::find($action['id']);
                    $actionModel->update($action);
                    $actionsIdsPut[] = $action['id'];
                }
            }
            foreach ( $actionsIdsHave as $actionIdExist ) {
                if ( !in_array($actionIdExist , $actionsIdsPut) ) {
                    CheckPageAction::destroy($actionIdExist);
                }
            }
        }
        return $this->apiReturn(true , 'ok' , compact('checkPage'));
    }

    /**
     * @param         $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function history($id,Request $request) {
        $historys = CheckPageHistory::where('cp_id' , $id);
        if ( $request->has('action_id') ) {
            $historys = $historys->where('action_id' , $request->input('action_id'));
        }
        $historys = $historys->orderBy('id','desc')->paginate();
        foreach ($historys as $history){
            if( starts_with($history->result,'/upload') ){
                $history->result = url($history->result);
            }
        }
        return $this->apiReturn(true , 'ok' , $historys);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id) {
        CheckPage::destroy($id);

        Session::flash('flash_message' , 'CheckPage deleted!');

        return redirect('admin/CheckPages');
    }
}
