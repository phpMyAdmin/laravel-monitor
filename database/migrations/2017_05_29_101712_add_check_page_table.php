<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCheckPageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'check_pages' ,
            function (Blueprint $table) {
                $table->increments('id');
                $table->string('title',30);
                $table->string('url',200);
                $table->string('user_agent',400);
                $table->string('crontab_period',30);
                $table->string('view_port' , 40);
                $table->string('config',100)->default('');
                $table->string('last_cost_time',10)->nullable()->default('');
                $table->tinyInteger('is_enabled')->default(0);
                $table->dateTime('last_run_at')->nullable()->default(null);
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('check_pages');
    }
}
