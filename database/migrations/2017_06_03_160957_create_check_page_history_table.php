<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheckPageHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'check_page_historys' ,
            function (Blueprint $table) {
                $table->increments('id');
                $table->integer('cp_id');
                $table->integer('action_id');
                $table->string('result',100);
                $table->string('last_result',100)->default('')->nullable();
                $table->string('diff_result',50)->default('')->nullable();
                $table->string('diff_percent',10)->default('')->nullable();
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('check_page_historys');
    }
}
